import React, { useState, useEffect } from 'react';
import active from './icons/active.svg';
import help from './icons/help-12.svg';
import arrow from './icons/arrow-right.svg';

import data from "./data.json";
import './App.css';

function App() {
  const [ currentTab, setTab ] = useState('1');
  const [ resources, setResources ] = useState([]);
  const [ actions, setActions ] = useState([]);

  useEffect(() => {
    resources.length === 0 && getData()
      .then(() => {
        setResources(data.resources);
        setActions(data.actions);
      })
  });

  const sending = m => new Promise(r => setTimeout(r, m));
  const getData = async () => {
    await sending(3000);
  };
  const currentItem = resources.find((item) => item.id === currentTab) || {};

  // Render block
  const renderPreloader = <div className="preloader">loading...</div>

  return (
    <div className="app-wrapper">
      <header>
        <p>Demo App</p>
      </header>
      <body>
        <div className="tabs-container">
          <div className="tabs-sidebar">
            <h4>Items</h4>
            {
              resources.length > 0 ? (
                resources.map((item) => {
                  let isActive = currentTab === item.id;

                  return (
                    <div
                      className={`tab-selector ${isActive ? 'active' : ''}`}
                      key={item.id}
                      onClick={() => setTab(item.id)}
                    >
                      <div>
                        <img src={active} alt="active"/>
                        <span>{item.name}</span>
                      </div>
                      { isActive && <img src={arrow} alt="arrow"/> }
                    </div>
                  )
                })
              ) : (
                renderPreloader
              )
            }
          </div>
          <div className="tabs-content">
            {
              currentItem.name && actions ? (
                <div className="tab-block">
                  <div className="tab-header">
                    <h4>{currentItem.name}</h4>
                  </div>
                  <div className="tab-body">
                    <div className="part-content left">
                      <div className="part-content-header">
                        <h5>GENERAL DETAILS</h5>
                        <img src={help}/>
                      </div>

                      <h6>NAME</h6>
                      <p><span>{currentItem.name}</span></p>
                      <h6>DESCRIPTION</h6>
                      <p><span>{currentItem.description}</span></p>
                      <h6>RESOURCE TYPE</h6>
                      <p><span>{currentItem.resourceType}</span></p>
                      <h6>PATH</h6>
                      <p><span>{currentItem.path}</span></p>

                    </div>
                    <div className="part-content right">
                      <div className="part-content-header">
                        <h5>PERMITTED ACTIONS</h5>
                        <img src={help}/>
                      </div>

                      <div className="actions">
                        {
                          currentItem.actionIds && currentItem.actionIds.map(id => {
                            let action = actions && actions.find(action => action.id === id);

                            return <p key={id}>{action && action.name || ''}</p>
                          })
                        }
                      </div>

                    </div>
                  </div>
                </div>
              ) : (
                renderPreloader
              )
            }
          </div>
        </div>
      </body>
    </div>
  );
}

export default App;
